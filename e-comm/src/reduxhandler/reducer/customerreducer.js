import {  updatecategories, userDetails } from "../action/actiontypes";
import image1 from '../../asserts/images/bread.png';
import image2 from '../../asserts/images/milk.jpg';
import image3 from '../../asserts/images/curd.jpeg';
import image4 from '../../asserts/images/carrot.jpg';
import image5 from '../../asserts/images/apple.jpg';

export const initialstate={
    customerName:"",
    categories :[
      { id: 1, label: "AllCategories",name:"allCatergories",indeterminateCheck:false, checked:true },
      { id: 2, label: "Bread",name:"bread",checked:true,indeterminateCheck:false },
      { id: 3, label: "Diary",name:"diary",checked:true,indeterminateCheck:false},
      { id: 4, label: "Friut",name:"fruit",checked:true,indeterminateCheck:false},
      { id: 5, label: "Vegetable",name:"vegetable",checked:true,indeterminateCheck:false},
    ],
    fliter : [
      { categories: "Bread",name:"wheatbread", title: "Wheat Bread 100%", Price: 100, image: image1,cartValue:0 },
      { categories: "Bread",name:"sweetbread", title: "Sweet Bread 100%", Price: 110, image: image1,cartValue:0 },
      { categories: "Diary",name:"curd", title: "Curd pure 100% ", Price: 100, image: image3,cartValue:0 },
      { categories: "Diary",name:"milk", title: "Milk pure 100%", Price: 122, image: image2,cartValue:0 },
      { categories: "Fruit",name:"apple", title: "Apple pure organic", Price: 122, image: image5,cartValue:0 },
      { categories: "Vegetable",name:"vegetable", title: "Carrot pure organic", Price: 122, image: image4,cartValue:0 },

    ],
    badges:0,
    total:0  ,
    active:false
}

const CustomerReducer = (state = initialstate, action) => {
    switch (action.type) {
       case userDetails:
      return {
        ...state,
        [action.payload.key]:action.payload.value
      }; 
      case updatecategories:
      return {
        ...state,
        categories:[...action.payload]  
      }; 
    default:
      return state;
  }
};
export default CustomerReducer;