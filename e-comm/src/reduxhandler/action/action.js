//import { initialstate } from "../reducer/customerreducer";
import {  updatecategories, userDetails, } from "./actiontypes";

export const add = (val) => {
    return (dispatch, getState) => {
      dispatch(commonFunction('customerName',val));
    };
  };
  export const update = (val) => {
    return (dispatch, getState) => {
      dispatch({type:updatecategories,payload:val});
    };
  };
  export const updatecartValue = (val) => {
    return (dispatch, getState) => {
      dispatch( commonFunction('fliter',val));
    };
  };
  export const updateBadgesValue = (val) => {
    return (dispatch, getState) => {
      dispatch(commonFunction('total',val));
      
     // dispatch({ type: updateBadges, payload: val });
    };
  };
  export const updateTotalValue = (val) => {
    return (dispatch, getState) => {
      dispatch( commonFunction('badges',val));
      //dispatch({ type: updateTotal, payload: val });
    };
  };
  export const activeUser = (val) => {
    return (dispatch, getState) => {
      dispatch(commonFunction('active',val));
    };
  };
  export const addProduct = (productDetails,addvalue) => {
    return (dispatch, getState) => {  
     const{name,cartValue} = productDetails;
      const{fliter}=getState();
      let totalValue=0;
      let index=fliter.findIndex(sc=>sc.name===name);
           fliter[index].cartValue=(cartValue+addvalue);
           let badgecount=fliter.filter(item => item.cartValue > 0).length ;
           // eslint-disable-next-line array-callback-return
           fliter.map((item)=> {
             totalValue=((totalValue + item.cartValue * item.Price));
           });
            
          
            dispatch( commonFunction('fliter',fliter));
            dispatch(commonFunction('total',totalValue));
            dispatch( commonFunction('badges',badgecount));
    };
  };
  export const clearData = (val) => {
    return (dispatch, getState) => { 
      const{fliter,categories}=getState();
      // eslint-disable-next-line array-callback-return
      categories.map((item)=>{
        item.checked=true ;
       item.indeterminateCheck=false});
      fliter.map((item)=>item.cartValue=0);
        
      dispatch(commonFunction('customerName',""));
   // dispatch({ type: updateCart, payload:fliter });
    dispatch(commonFunction('categories',categories));
      
    dispatch( commonFunction('fliter',fliter));
    dispatch(commonFunction('total',0));
    dispatch( commonFunction('badges',0));
    dispatch(commonFunction('active',false));
    };
  };
  export const updateCustomerDetail = (customerDetail) => {
    return (dispatch, getState) => {
      const{fliter,badges,total,customerName,categories,}=customerDetail;
      dispatch(commonFunction('customerName',customerName));
    dispatch(commonFunction('categories',categories));  
    dispatch( commonFunction('fliter',fliter));
    dispatch(commonFunction('total',total));
    dispatch( commonFunction('badges',badges));
    dispatch(commonFunction('active',true));

    };
  };
  export const clearCart = () => {
    return (dispatch, getState) => {
      const{fliter}=getState();
      fliter.map((item)=>item.cartValue=0);
     
      
      
    dispatch( commonFunction('fliter',fliter));
    dispatch(commonFunction('total',0));
    dispatch( commonFunction('badges',0));
    };
  };
  export const commonFunction = (key,value) => {
    return (dispatch, getState) => {
      dispatch({ type: userDetails, payload: {key,value} });
    };
  };