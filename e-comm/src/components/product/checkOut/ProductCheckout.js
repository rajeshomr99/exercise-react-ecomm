import {connect} from 'react-redux';
import { useState } from 'react';
import { clearCart, update, updateBadgesValue, updatecartValue } from '../../../reduxhandler/action/action';
import { Cartpath, Productpath } from '../../../Router/routerPath';
import "./ProductCheckout.css" 
const ProductCheckOut = (props) => {
    const{fliter,total,categories  ,badges}=props;
    const[modal,setModal]=useState(false)
    const handleChange=(path)=>{
        console.log(path);
        props.history.push(path);

    }
    const clearCartdetails=()=>{
      // eslint-disable-next-line array-callback-return
      categories.map((item)=>{
        item.checked=true;
        item.indeterminateCheck=false;
      })
        props.clearCart()
        props.updateIndexDetails(categories)
        props.history.push(Productpath);
    }
    const modalOpen=()=>{
      setModal(true);
    }
    //let total=0;
    
  return (
    <>
    <div className="checkOut">
      <div className="goBack">
        <input type="button" value="Back to cart" className="goback" onClick={()=>handleChange(Cartpath)} />
      </div>
      <div className="checouttable">
          <div className="checkouthead">Order Summary</div>
          <div className="selectedIItem">{`You have ${badges} selected Item`}</div>
          <table>
              <tbody>
              {fliter.map((item)=>(
                         //count=count+(item.cartValue*item.Price)&&   
                        item.cartValue > 0 &&
                        <tr className="cartrow">
                           <td className="noOfItem"><b>{`${item.cartValue} X ${item.title}`}</b></td>
                           <td className="tablename"><b>{`$${item.cartValue*item.Price}`}</b></td>
                           </tr>
              ))}
               <tr className="cartrow">
                   <td><b>Total Price</b></td>
                   <td><b>{`$${total}`}</b></td>
                   </tr>     
              </tbody>
          </table>
           </div>
           <div className="submitbutton">
            <input type="submit"  onClick={()=>modalOpen()} value="Place Order" className="checkout"/>
        </div>
        
    </div>
    {modal&&
      <div className="modalMain">
      <div className="modal">
          <div className="modalText"> Thank you for placing the order</div>
          <div className="goHome" onClick={()=>clearCartdetails()}>Okay</div>
      </div>
      </div>
      }
      </>
  );
};
const mapStateToPrpops = (state) => {
    return state;
  };
  const mapDispacterToProps = (dispatch) => {
    return {
        updateCount: (val)=>dispatch(updatecartValue(val)),
        clearCart: ()=>dispatch(clearCart()),
        updateIndexDetails: (val) => dispatch(update(val)),
        updateBadges: (val)=>dispatch(updateBadgesValue(val)),
     
    };
  };
export default connect(mapStateToPrpops, mapDispacterToProps) (ProductCheckOut);
//export default ProductCheckOut;
