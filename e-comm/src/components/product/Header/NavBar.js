import {  Cartpath, loginpath  } from "../../../Router/routerPath";
import "./navbar.css";
const Header = (props) => {
  const { customerName, back, badge } = props
  return (
    <div className="nav_bar">
      <div className="titleName">Welcometo React E-Commerece Shopping Mart</div>
      <div className="actionButton">
        <span className="userName">{`Hello ${customerName}`}</span>
        <span className="logout" onClick={() => back(loginpath,true)}>
          <i class="fas fa-sign-out-alt"></i>
        </span>
        <span className="cartSymbol">
          <i
            class="fas fa-cart-plus"
            onClick={() => back(Cartpath,false)}
            style={!badge ? { pointerEvents: "none", color: "lightgray" } : {}}
          >
            <span className="badge">{badge}</span>
          </i>
        </span>
      </div>
    </div>
  );
};
export default Header;
