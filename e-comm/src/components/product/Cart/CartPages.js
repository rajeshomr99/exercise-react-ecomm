import "./CartPages.css";
import { connect } from "react-redux";
import { CheckOutPath, Productpath } from "../../../Router/routerPath";
import { addProduct, clearCart } from "../../../reduxhandler/action/action";
const CartPage=(props)=>{
    const{fliter,badges,total,updateAll,clearCart}=props;
    const handleChange=(path)=>{
        
        if(path===CheckOutPath&&total===0){
            alert("Add the product to cart")
        }
        else{
            props.history.push(path);
        }
      //  

    }
    
return(
    <div className="cartpages">
        <div className="goBack">
            <input type="button" value="Back to product" className="goback" onClick={()=>handleChange(Productpath)} />
        </div>
        <div className="heading"><b>Shopping Cart</b></div>
        <div className="tableContent">
            <div className="tableHead">
                <div className="selectedItem">{`You have  item ${badges} in your Cart`}</div>
                <input type="button" className="clear"onClick={clearCart}  value='clear shopping cart'/>
            </div>
            <table className="cartTable">
                <tbody>
                    <tr>
                        <td className="imagestag"></td>
                        <td className="product">Product</td>
                        <td className='quantity'>Quantity</td>
                        <td className="price">Price</td>
                    </tr>
                    {fliter.map((item)=>(
                        item.cartValue > 0 &&
                        <tr className="cartrow">
                           <td className="tableimage"><img src={item.image} className="tableImg" alt="images" /></td>
                           <td className="tablename">{item.title}</td>
                           <td className="add/delete">
                            <div className="addordelete">   
                           <input type="button" className="addDelte" value="-" onClick={(e) => updateAll(item, -1)} />
                            <div className="vaue">{item.cartValue}</div>
                            <input type="button" className="addDelte" value="+" onClick={(e) => updateAll(item, 1)}  />
                            </div>  </td>
                            <td className="price">{`$${item.Price * item.cartValue}`}</td>
                        </tr>
                        ))} 
                        <tr>
                            <td className="totalprice">Total Price</td>
                            <td className="amount">{`$${total}`}</td>
                        </tr>
                </tbody>
            </table>
        </div>
        <div className="submitbutton">
            <input type="submit"  value="CheckOut" onClick={()=>handleChange(CheckOutPath)} className="checkout"/>
        </div>
    </div>
);
}
const mapStateToPrpops = (state) => {
    return state;
  };
  const mapDispacterToProps = (dispatch) => {
    return {
        clearCart: ()=>dispatch(clearCart()),
        updateAll: (productDetails,addvalue)=>dispatch(addProduct(productDetails,addvalue))
    };
  };
export default connect(mapStateToPrpops, mapDispacterToProps) (CartPage);