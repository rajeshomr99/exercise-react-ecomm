const ProductIformation = (props) => {
  const { input, changevalue, productDetails } = props;
  return(
    <div className="productList">
      {productDetails.map((item) => (
    <>
      {input.find((sc) => sc.name === item.categories.toLowerCase())
        ?.checked && (
        <div className="productMain">
          <div className="productHower">
            <div className="tiltename">{item.title}</div>
            <div className="image">
              <img className="productImges" src={item.image} alt="images"/>
            </div>
            <div className="productname">
              <span className="productName">{item.name}</span>
              <br></br>
              <span>{`$${item.Price}`}</span>
            </div>
          </div>
          {!item.cartValue && (
            <div className="cart">
              <input
                type="button"
                className="cartbutton"
                value="Cart"
                onClick={(e) => changevalue(item, 1)}
              />
            </div>
          )}
          {item.cartValue > 0 && (
            <div className="add">
              <input
                type="button"
                value="-"
                className="addDel"
                onClick={(e) => changevalue(item, -1)}
              />
              <div className="vaue">{item.cartValue}</div>
              <input
                type="button"
                value="+"
                className="addDel"
                onClick={(e) => changevalue(item, 1)}
              />
            </div>
          )}
        </div>
      )}
    </>
  ))}
    </div>
  ); 
};
export default ProductIformation;
