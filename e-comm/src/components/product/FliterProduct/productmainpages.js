import ProductChecK from "./productCheckBox";
import ProductIformation from "./productInformation";
import "./productmainpages.css";
import { useEffect } from "react";
import {connect} from 'react-redux';
import { addProduct, update } from "../../../reduxhandler/action/action";
import { loginpath } from "../../../Router/routerPath";

const ProductHome=(props)=>{
    const{categories,fliter,updateAll,active}=props;
    useEffect(() => {
      if(!active){
        props.history.push(loginpath);
      }
    }, [active, props.history])
    const handleChange=(e,productFliter)=>{
      const{id,checked}=productFliter;


      if(id===1 && checked===false){
        categories.map((item)=>item.checked=true)
      }
      else{
        if(id !== 1 && (categories.filter(item => item.checked===true).length > 1 || checked===false)){
        categories[id-1].checked=!checked?true:false;
      }
    } 
      let temp=categories.some(sc=>sc.id !== 1 && sc.checked===false);
        categories[0].checked=!temp?true:false; 
        categories[0].indeterminateCheck=temp;
      
      
      props.updateIndexDetails(categories)
    }
      // const handleChange1=(name,value,addvalue)=>{
      //   let totalValue=0;
      //     let index=fliter.findIndex(sc=>sc.name===name);
      //     fliter[index].cartValue=(value+addvalue);
      //     let badgecount=fliter.filter(item => item.cartValue > 0).length ;
      //     fliter.map((item)=> {
      //       totalValue=((totalValue + item.cartValue * item.Price));
      //      });
      //     props.updateCount(fliter);
      //     props.updateTotalValue(totalValue);
      //     props.updateBadges(badgecount); 
      // }
   return(
        <div className="product-checkbox">
            <ProductChecK  input={categories} changevalue={handleChange}/>
            <ProductIformation input={categories} productDetails={fliter} changevalue={updateAll} />
        </div>
    )
}   
const mapStateToPrpops = (state) => {
    return state;
  };
  const mapDispacterToProps = (dispatch) => {
    return {
      updateIndexDetails: (val) => dispatch(update(val)),
      updateAll: (productDetails,addvalue)=>dispatch(addProduct(productDetails,addvalue))
    };
  };
export default connect (mapStateToPrpops,mapDispacterToProps) (ProductHome);