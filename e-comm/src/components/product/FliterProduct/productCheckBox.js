import "./productmainpages.css";

const ProductChecK = (props) => {
  const { input, changevalue } = props;
  return (
    <div className="checkmain">
      {input.map((item) => (
        <div className={item.name}>
          <span>{item.label}</span>
          <input
            type="checkbox"
            checked={item.checked}
            ref={ee => {
                if (ee) {
                  console.log(ee)
                  ee.indeterminate = item.indeterminateCheck;
                }
              }}
            onChange={(e) => changevalue(e,item)}
          />
        </div>
      ))}
    </div>
  );
};
export default ProductChecK;
