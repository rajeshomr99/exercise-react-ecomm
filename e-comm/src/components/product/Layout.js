import Header from "./Header/NavBar";
import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import { routers } from "../../Router/routes";
import { Layoutpath, Productpath } from "../../Router/routerPath";
import { activeUser } from "../../reduxhandler/action/action";

const Layout = (props) => {
  const { customerName,badges } = props;  
  const handleEvent = (path,active) => {
    if(active){
      props.activeUser(false);
    }
    props.history.push(path);
  };
  return (
    <div className="userpage">
      <Header customerName={customerName} badge={badges} back={handleEvent} />
      <Switch>
      <Redirect exact  from={Layoutpath} to={Productpath}/> 
        {routers.map((item) => (
          <Route
            path={item.path}
            exact={item.exact }
            component={item.component}
          />
        ))}
      </Switch>
    </div>
  );
};
const mapStateToPrpops = (state) => {
  return state;
};
const mapDispacterToProps = (dispatch) => {
  return {
   
    activeUser: (val)=>dispatch(activeUser(val)),
   // updateCustomerDetail:(val)=>dispatch(updateCustomerDetail(val))
   
  };
};
export default connect(mapStateToPrpops, mapDispacterToProps)(Layout);