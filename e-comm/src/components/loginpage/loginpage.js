import { useState } from "react";
import { Layoutpath } from "../../Router/routerPath";
import { Input } from "../commomComponent/input_tag"
import "./loginpage.css";
import { connect } from 'react-redux';
import { activeUser, add, clearData, updateCustomerDetail } from "../../reduxhandler/action/action";
import Background from '../../asserts/images/ec1.jpg'

 const LoginPage=(props)=>{
    const[username,setname]=useState({name:"",errorMsg:""});
   // console.log(props);
   props.activeUser(false);
    const handleChange=(event)=>{
        setname({...username,name:event.target.value,errorMsg:""})
    }
    const handleSubmit=(event)=>{
        event.preventDefault();
        if(!username.name||!username.name.match(/^[a-zA-Z]+$/)){
            setname({...username,errorMsg:`Enter the valid name`})
        }
        else{
            let temporaryUser=JSON.parse(localStorage.getItem("userdetail"))|| [];
            props.clearData();
            let usercheck=temporaryUser.some(sc=>sc.customerName===username.name);
            if(usercheck){
                const index=temporaryUser.findIndex((item)=>item.customerName===username.name);
                props.updateCustomerDetail(temporaryUser[index])
            }
            else{
                props.additem(username.name)
            }
            props.activeUser(true);
            props.history.push(Layoutpath);
        }
    }
    return(
        <div className="loginPage"   style={{  
            backgroundImage: `url(${Background})`,
            backgroundPosition: 'center',
            backgroundSize: '100% 100%',
            backgroundRepeat: 'no-repeat'
          }}>
        <div className="loginPageForm" >
        <div className="loginTitle">E-Commence Login</div>
            <form  onSubmit={handleSubmit}>
                
            <Input changeValue={handleChange} name="Username" error={username.errorMsg} />
            <input type="submit" value="Submit" />
            </form>
        </div>
        </div>
    )
}
const mapStateToPrpops = (state) => {
    return state;
  };
  const mapDispacterToProps = (dispatch) => {
    return {
        clearData: ()=>dispatch(clearData()),
      additem: (val) => dispatch(add(val)),
      activeUser: (val)=>dispatch(activeUser(val)),
      updateCustomerDetail:(val)=>dispatch(updateCustomerDetail(val))
     
    };
  };
  export default connect(mapStateToPrpops, mapDispacterToProps)(LoginPage);