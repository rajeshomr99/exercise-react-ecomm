
import { Route, Switch } from " ";
import { useEffect } from "react";
import { connect } from "react-redux";
import { routerfunction } from "../Router/routes";
import { updateCustomerDetail } from "../reduxhandler/action/action";
import { loginpath } from "../Router/routerPath";

const IndexDetails = (props) => {
  useEffect(() => {
    console.log("rajesh");
    let temporaryUser=JSON.parse(localStorage.getItem("userdetail"));
    if(temporaryUser !== null){
    let stablePage=temporaryUser.some(sc=>sc.active===true);
    if(stablePage){
      const index=temporaryUser.findIndex((item)=>item.active===true);
      props.updateCustomerDetail(temporaryUser[index]);

    }
   
  }
    
  }, [])
  return (
    <div className="mainContainer">
       <Switch>
        {routerfunction.map((item) => (
          <Route
            key={item.path}
            path={item.path}  
            exact={item.exact}
            component={item.component}
          />
        ))}
      </Switch>
    </div>
  );
};
const mapStateToPrpops = (state) => {
  return state;
};
const mapDispacterToProps = (dispatch) => {
  return {
    updateCustomerDetail:(val)=>dispatch(updateCustomerDetail(val))
   
  };
};
export default connect(mapStateToPrpops, mapDispacterToProps)(IndexDetails);

