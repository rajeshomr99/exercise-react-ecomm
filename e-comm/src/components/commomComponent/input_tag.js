export const Input = (props) => {
    const{changeValue,error,name}=props
  return (
    <>
      <input type="text" onChange={(event)=>changeValue(event)} maxLength="15" placeholder={name} />
      <div className="errorMsg">{error}</div>
    </>
  );
};
