import { createStore,applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import CustomerReducer from "./reduxhandler/reducer/customerreducer";

const store = createStore(CustomerReducer,applyMiddleware(thunk));
store.subscribe(()=>{
    let userdetail=store.getState();
    let temporaryUser=JSON.parse(localStorage.getItem("userdetail"))|| [];
    let usercheck=temporaryUser.some((item)=>item.customerName===userdetail.customerName);
    if(usercheck){
        
        let index=temporaryUser.findIndex((item)=>item.customerName === userdetail.customerName);
        temporaryUser.splice(index,1,userdetail);
        //console.log(temporaryUser); 
        localStorage.setItem("userdetail", JSON.stringify(temporaryUser));
        
    }
    else
    {   
        if(userdetail.customerName !==""){
        temporaryUser.push(userdetail);
        localStorage.setItem("userdetail", JSON.stringify(temporaryUser));
    }
    }
    });
    


export default store;
