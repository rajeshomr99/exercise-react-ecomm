import LoginPage from "../components/loginpage/loginpage";
import { Cartpath,  CheckOutPath,  Layoutpath, loginpath,Productpath } from "./routerPath";
import Layout from "../components/product/Layout";
import ProductHome from "../components/product/FliterProduct/productmainpages";
import CartPage from "../components/product/Cart/CartPages";
import ProductCheckOut from "../components/product/checkOut/ProductCheckout";

export const routerfunction = [
    {
      path: loginpath,
      exact: true,
      component: LoginPage,    
    },
    {
        path: Layoutpath,
        component: Layout,    
      }                                                               
    
  ];
  export const routers = [
    {
      path: Productpath,
       exact: true,
      component: ProductHome,    
    },    
    {
      path: Cartpath,
       exact: true,
      component: CartPage,    
    } ,
    {
      path:CheckOutPath,
      exact:true,
      component:ProductCheckOut
    }                                                 
    
  ];
  